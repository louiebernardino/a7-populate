//Declare dependencies
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

//Define your schema
const taskSchema = new Schema(
	{
		description: {
			type: String,
			required: true,
			maxlength: 100
		},
		memberId: {
			type: mongoose.Schema.Types.ObjectId,
			required: true,
			ref: "Member"

		},
		isCompleted: {
			type: Boolean,
			default: false
		}
	},
	{
		timestamps: true
	}
);

//Export your model
module.exports = mongoose.model("Task", taskSchema);