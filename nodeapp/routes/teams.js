//Declar dependencies and model
const Team = require("../models/teams")
const express = require("express")
const router = express.Router() //to handle routing
const auth = require("../middleware/auth")


//Create Routes/Endpoints


//CREATE A TEAM
router.post("/", async (req, res) => {
	// console.log("test");
	// return res.send(req.body);
	const team = new Team(req.body);
	try {
		await team.save()
		res.send(team)
	} catch(e) {
		res.status(400).send(e)
	}
})

//GET ALL TEAMS
router.get("/", async (req, res) => {
	try {
		const teams = await Team.find()
		res.status(200).send(teams)
		console.log("no return")
	} catch(e) {
		return res.status(404).send(e)
		console.log("after return 2")
	}
})

//GET ONE TEAM AND ALL ITS MEMBERS (THE TEAM OF THE ONE LOGGED)
router.get("/:id", auth, async (req, res) => {

	const _id = req.params.id
	const match = {} //instantiating an object named match
	const sort = {} //instantiating an object named sort
	if(req.query.position) {
		match.position = req.query.position //this line gives value to the object match (the value would be the one on the url)
	}

	if(req.query.sortBy) {
		const parts = req.query.sortBy.split(":")
		sort[parts[0]] = parts[1] === "desc" ? -1 : 1
	}

	console.log(sort)
	try {
		const team = await Team.findById(_id)
		//if team is {}
		if(!team) {
			//not yet working
			return res.status(404).send(e)
		}
		//populate team with its members
		await team.populate({path: "members", match, options: {
			limit: parseInt(req.query.limit),
			skip: parseInt(req.query.skip), sort}}).execPopulate()
		return res.send(team.members) //return the members of the team


	} catch(e) {
		console.log("error 500")
		return res.send(e)
	}
})

//UPDATE A TEAM
router.patch("/:id", async (req, res) => {
	// return res.send("update a team");
	const _id = req.params.id

	try {
		const team = await Team.findByIdAndUpdate(_id, req.body, {new: true})
		if(!team) {
			return res.status(404).send(e)
		}
		res.send(team)
	} catch(e) {
		return res.status(500).send(e)
	}
})

//DELETE A TEAM
router.delete("/:id", async (req, res) => {
	// return res.send("delete a team");
	const _id = req.params.id

	//Mongoose Models Query
	Team.findByIdAndDelete(_id)
		.then((team) => {
			if(!team) {
				return res.send(404).send(e)
			}
			return res.send(team)
		})
		.catch((e) => {
			return res.status(500).send(e)
		})
	try {
		const team = await Team.findByIdAndDelete(_id)
		if(!team) {
			return res.status(404).send("Team doesn't exist")
		}
		res.send(team)
	} catch(e) {
		res.status(500).send(e.message)
	}
})

module.exports = router